// ---Dependencys
import { Row, Col } from 'antd';
import { ReactElement } from 'react';

// ----------------------------------------COMPONENT----------------------------------------
export default function Content(): ReactElement {
  return (
    <Row>
      <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8} className="escuelas">
        <p>UCM</p>
      </Col>
      <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8} className="escuelas">
        <p>UNAM</p>
      </Col>
      <Col xs={24} sm={24} md={8} lg={8} xl={8} xxl={8} className="escuelas">
        <p>UBA</p>
      </Col>
      <Col span={24} className="contenido">
        <p>
          El congreso surge de una preocupación regional sobre el estado que guardan las relaciones tributarias en los diferentes marcos nacionales y las problemáticas que subyacen a esta: digitalización de la economía, criterios económicos, límites y vulneración de los derechos fundamentales de los contribuyentes, facultades de las autoridades administrativas y criterios de aplicación de justicia en materia tributaria.
        </p>
        <p>
          Frente a esta preocupación las universidades convocantes han decidido crear un espacio donde convergen nuevas voces para plantear el estado de la cuestión desde diversas perspectivas, fomentar el debate y realizar propuestas.
          {' '}
        </p>
      </Col>
      <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6} className="icono-mapa">
        <a
          rel="noreferrer"
          target="_blank"
          href="https://goo.gl/maps/Q4phAAyGo28NSc3y7"
        >
          <img
            src="\images\mapa.png"
            alt="lo sentimos la imagen no se pudo encontrar"
          />
        </a>
      </Col>
      <Col xs={12} sm={6} md={6} lg={6} xl={6} xxl={6} className="texto-mapa">
        <a
          rel="noreferrer"
          target="_blank"
          href="https://goo.gl/maps/Q4phAAyGo28NSc3y7"
        >
          <h1>Ciudad Universitaria, CDMX</h1>
        </a>
      </Col>
      <Col
        xs={12}
        sm={6}
        md={6}
        lg={6}
        xl={6}
        xxl={6}
        className="icono-calendario"
      >
        <img
          src="\images\calendario.png"
          alt="lo sentimos la imagen no se pudo encontrar"
        />
      </Col>
      <Col
        xs={12}
        sm={6}
        md={6}
        lg={6}
        xl={6}
        xxl={6}
        className="texto-calendario"
      >
        <h1>
          18, 19 y 20
          <br />
          {' '}
          de mayo
          <br />
          2022
        </h1>
      </Col>
    </Row>
  );
}
