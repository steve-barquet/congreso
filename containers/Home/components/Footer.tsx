import { ReactElement } from 'react';
import { Row, Col } from 'antd';

export default function Footer(): ReactElement {
  return (
    <Row>
      <Col
        xs={24}
        sm={8}
        md={8}
        lg={8}
        xl={8}
        xxl={8}
        className="fb"
      >
        <a href="https://api.whatsapp.com/send/?phone=525541851471" target="_blank" rel="noopener noreferrer">
          <img src="\images\WhatsApp.png" alt="lo sentimos la imagen no se pudo encontrar" />
        </a>
        <p>55 4185 1471</p>
      </Col>
      <Col
        xs={24}
        sm={8}
        md={8}
        lg={8}
        xl={8}
        xxl={8}
        className="gaceta"
      >
        <h1>
          <a target="_blank" href="https://www.gaceta.unam.mx" rel="noreferrer">
            <img src="\images\gaceta.jpg" alt="lo sentimos la imagen no se pudo encontrar" />
          </a>
        </h1>
      </Col>
      <Col
        xs={24}
        sm={8}
        md={8}
        lg={8}
        xl={8}
        xxl={8}
        className="tl"
      >
        <a rel="noreferrer" target="_blank" href="https://t.me/CIDTMX">
          <img src="\images\telegram.png" alt="lo sentimos la imagen no se pudo encontrar" />
        </a>
      </Col>
    </Row>
  );
}
