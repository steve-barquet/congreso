// ---Dependencys
import { ReactElement } from 'react';
// ---Components
import Title from 'Cont/Home/components/Title';
import Content from 'Cont/Home/components/Content';
import Certificado from 'CComps/Certificado';
import Cuota from 'Cont/Programa/components/Cuota';
import Footer from './components/Footer';

// ----------------------------------------COMPONENT----------------------------------------
export default function Home(): ReactElement {
  return (
    <div className="home-container">
      <Title />
      <Content />
      <Footer />
      <Certificado />
      <Cuota />
    </div>
  );
}
