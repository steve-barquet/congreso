// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';

// ----------------------------------------COMPONENT----------------------------------------
export default function Cuota(): ReactElement {
  return (
    <Row className="cuota">
      <Col xs={24} sm={24} md={24} lg={10} xl={10}>
        <h1>CUOTA DE RECUPERACION</h1>
      </Col>
      <Col xs={24} sm={24} md={24} lg={14} xl={14}>
        <table>
          <tr>
            <td>PRESENCIAL</td>
            <td>$1,000.00 MX</td>
          </tr>
          <tr>
            <td>AULA Virtual</td>
            <td>$800.00 MX</td>
          </tr>
          <tr>
            <td>COMUNIDAD UNAM</td>
            <td>$600.00 MX</td>
          </tr>
        </table>
      </Col>
    </Row>
  );
}
