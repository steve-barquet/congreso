/* eslint-disable react/require-default-props */
// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';

// ---Types
interface Props{
    tituloBold?: string;
    titulo?: string;
    initHrs: string;
    evento: string;
    participante: string;
    puesto?:string;
}
// ----------------------------------------COMPONENT----------------------------------------
export default function ProgramList(props:Props): ReactElement {
  const {
    evento, initHrs, participante, titulo, tituloBold, puesto
  } = props;
  return (
    <Col className="event-list">
      <p>
        <b>{tituloBold}</b>
        <br />
        <span>{titulo}</span>
      </p>
      <Row>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <h3>{evento}</h3>
          <h4>{participante}</h4>
          <h2>{puesto}</h2>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <h5>{initHrs}</h5>
        </Col>
      </Row>
    </Col>
  );
}
