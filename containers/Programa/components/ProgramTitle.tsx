// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';

// ----------------------------------------COMPONENT----------------------------------------
export default function ProgramTitle(): ReactElement {
  return (
    <Row className="title" justify="center">
      <Col>
        <h2>CONGRESO IBEROAMERICANO DE DERECHO TRIBUTARIO</h2>
        <h1>Programa de actividades</h1>
      </Col>
    </Row>
  );
}
