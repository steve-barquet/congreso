/* eslint-disable react/require-default-props */
// ---Dependencys
import { ReactElement } from 'react';
import { Col } from 'antd';

import ProgramTitle from 'Cont/Programa/components/ProgramTitle';
// ----------------------------------------COMPONENT----------------------------------------
export default function ProgramTop(): ReactElement {
  return (
    <Col className="top-list">
      <ProgramTitle />
      <h1>18 de mayo</h1>
      <h2>Acto Inaugural - 10:00 AM</h2>
      <tr>
        <td> Doctor Raúl Contreras Bustamante - Director de la Facultad de Derecho de la UNAM</td>
        <td> Doctora Guadalupe Fernandez - Jefa de la División de Posgrado de la Facultad de Derecho de la UNAM</td>
      </tr>
      <tr>
        <td> Doctor Juan Manuel Álvarez Echagüe - Director del Observatorio de Derecho Penal Tributario de la Universidad de Buenos Aires</td>
        <td> Doctor Luis Manuel Alonso González - Jefe del Departamento de Derecho Financiero y Tributario de la Universidad de Barcelona</td>
      </tr>
      <tr>
        <td> Doctor José Manuel Almudí Cid - Director de la Escuela de Práctica Jurídica de la Universidad Complutense de Madrid</td>
        <td> Maestro Ambrosio Michel Higuera - Director Comité Organizador del Congreso</td>
      </tr>
    </Col>
  );
}
