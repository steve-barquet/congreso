// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row, Button } from 'antd';
import Link from 'next/link';

// ----------------------------------------COMPONENT----------------------------------------
export default function Footer(): ReactElement {
  return (
    <Row className="footer-programa" justify="space-around">
      <Col>
        <h1>
          Congreso
          <br />
          Iberoamericano
          <br />
          de Derecho Tributario
        </h1>
        <Link href="/InscripcionPage">
          <Button>INSCRIPCION</Button>
        </Link>
      </Col>
      <Col>
        <p>
          <a href="/files/programa_detalle.pdf" download>
            PROGRAMA
          </a>
          <br />
          <a href="/files/Ficha_De_Pago.pdf" download>
            FICHA DE PAGO
          </a>
          <br />
          <Link href="/InscripcionPage">
            DUDAS Y PREGUNTAS
          </Link>
          <br />
          MATERIALES DE APOYO
          <br />
          AULA VIRTUAL UNAM
          <br />
          SOLICITUD DE CONSTACIA DIGITAL
          <br />
          <a href="/files/Aviso_de_privacidad_Consultores_y _Asesores_CIDT.pdf" download>
            AVISO DE PRIVACIDAD
          </a>
        </p>
      </Col>
      <Col className="cidt-footer">
        <span>CIDT</span>
      </Col>
    </Row>
  );
}
