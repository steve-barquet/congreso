// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';
// ---Components
import PonentesListPresi from 'Cont/Ponentes/components/componentes/PonentesListPresi';
// ---Others
import { presidencia } from 'Others/cv';

// ----------------------------------------COMPONENT----------------------------------------
export default function PresidentList(): ReactElement {
  return (
    <Row className="ponentes-presidencia">
      <Col span={24}>
        <h1>Presidencia</h1>
        <hr />
      </Col>
      {presidencia.map((data) => (
        <PonentesListPresi
          key={data.nombre}
          nombre={data.nombre}
          img={data.img}
          curriculum={data.curriculum}
        />
      ))}
    </Row>
  );
}
