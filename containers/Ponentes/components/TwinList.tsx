// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';
// ---Components
import PonentesListPresi from 'Cont/Ponentes/components/componentes/PonentesListPresi';
// ---Others
import { comitOrga, comitAcade } from 'Others/cv';

// ----------------------------------------COMPONENT----------------------------------------
export default function TwinList(): ReactElement {
  return (
    <Row className="ponentes-comite">
      <Col span={12}>
        <Col span={24}>
          <h1>Comité Organizador</h1>
          <hr />
        </Col>
        {comitOrga.map((data) => (
          <PonentesListPresi
            nombre={data.nombre}
            img={data.img}
            curriculum={data.curriculum}
          />
        ))}
      </Col>
      <Col span={12}>
        <Col span={24}>
          <h1>Comité Académico</h1>
          <hr />
        </Col>
        {comitAcade.map((data) => (
          <PonentesListPresi
            nombre={data.nombre}
            img={data.img}
            curriculum={data.curriculum}
          />
        ))}
      </Col>
    </Row>
  );
}
