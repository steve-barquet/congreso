// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';
// ---Components
import PonentesListPresi from 'Cont/Ponentes/components/componentes/PonentesListPresi';
// ---Others
import { conferencistas } from 'Others/cv';

// ----------------------------------------COMPONENT----------------------------------------
export default function ConferencistasList(): ReactElement {
  return (
    <Row className="ponentes-presidencia">
      <Col span={24}>
        <h1>Conferencistas</h1>
        <hr />
      </Col>
      {conferencistas.map((data) => (
        <PonentesListPresi
          nombre={data.nombre}
          img={data.img}
          curriculum={data.curriculum}
        />
      ))}
    </Row>
  );
}
