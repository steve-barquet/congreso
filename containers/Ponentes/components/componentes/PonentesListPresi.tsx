/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
// ---Dependencys
import { ReactElement, useState } from 'react';
import { Row, Avatar } from 'antd';
// ---Components
import CvCard from 'containers/Ponentes/components/componentes/CvCard';
// ---Global Comps
import InvisibleButton from 'CComps/InvisibleButton';

// ----Interface
interface Props {
    nombre: string;
    curriculum: string;
    img: string;
}

// ----------------------------------------COMPONENT----------------------------------------
export default function PonentesListPresi(props:Props): ReactElement {
  const {
    nombre, curriculum, img
  } = props;
  const [modalcito, dispatch] = useState(false);

  const openModal = () => {
    dispatch(true);
  };
  const closeModel = () => {
    dispatch(false);
  };

  return (
    <>
      {modalcito && (<CvCard handleCancel={closeModel} img={img} nombre={nombre} curriculum={curriculum} />)}
      <InvisibleButton isVisible={modalcito} callback={closeModel} />
      <Row className="presi-styles" onClick={openModal} justify="center">
        <Avatar
          src={img === '' ? '/images/pete-random.jpeg' : img}
          size={{
            xs: 85, sm: 85, md: 85, lg: 120, xl: 130, xxl: 135
          }}
        />
        <h2>{nombre}</h2>
      </Row>
    </>
  );
}
