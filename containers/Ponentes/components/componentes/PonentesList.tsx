// ---Dependencys
import { ReactElement, useState } from 'react';
import {
  Col, Row, Avatar, Modal, Button
} from 'antd';

// ----Interface
interface Props {
    cssStyles:string;
    nombre: string;
    curriculum: string;
    img: string;
}

// ----------------------------------------COMPONENT----------------------------------------
export default function PonentesList(props:Props): ReactElement {
  const {
    nombre, curriculum, img, cssStyles
  } = props;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <Row className={cssStyles} justify="center">
      <Col xs={24} sm={24} md={8} lg={8} xl={8}>
        <>
          <Avatar
            src={img}
            size={{
              xs: 32, sm: 40, md: 60, lg: 80, xl: 100, xxl: 120
            }}
          />
          <Button onClick={showModal}>{nombre}</Button>
        </>
        <Modal title="Curriculum" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
          <p>{curriculum}</p>
        </Modal>
      </Col>
    </Row>
  );
}
