// ---Dependencys
import { ReactElement } from 'react';
import {
  Avatar, Row, Col, Button
} from 'antd';

// ----Interface
interface Props {
  img: string;
  nombre: string;
  curriculum: string;
  handleCancel: ()=>void
}
// ------------------------------------------ COMPONENT-----------------------------------------
export default function CvCard(props: Props): ReactElement {
  const {
    img, nombre, curriculum, handleCancel
  } = props;

  return (
    <div className="cv-card">
      <h1>Resumen Profesional</h1>
      <Row justify="center">
        <Col xs={24} sm={24} md={10} lg={10} xl={10}>
          <Avatar
            src={img === '' ? '/images/pete-random.jpeg' : img}
            size={{
              xs: 105, sm: 105, md: 105, lg: 140, xl: 150, xxl: 155
            }}
            style={{ margin: '70px' }}
          />
          <h2>{nombre}</h2>
        </Col>
        <Col xs={24} sm={24} md={14} lg={14} xl={14}>
          <p>{curriculum}</p>
        </Col>
        <Col span={18}>
          <Button style={{ width: '100%' }} onClick={handleCancel}>
            Cerrar
          </Button>
        </Col>
      </Row>
    </div>
  );
}
