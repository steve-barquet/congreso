// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';
// ---Components
import PresidentList from 'Cont/Ponentes/components/PresidentList';
import TwinList from 'Cont/Ponentes/components/TwinList';
import ConferencistasList from 'Cont/Ponentes/components/ConferencistasList';

// ----------------------------------------COMPONENT----------------------------------------
export default function PonentesCont(): ReactElement {
  return (
    <Row className="ponentes-container">
      <Col span={24}>
        <PresidentList />
      </Col>
      <Col span={24}>
        <TwinList />
      </Col>
      <ConferencistasList />
    </Row>
  );
}
