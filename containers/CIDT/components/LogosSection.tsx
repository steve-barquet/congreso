// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';
// ---Redux
import { useSelector } from 'react-redux';
import { ReduxState } from '@Redux/globalReducers';

// ----------------------------------------COMPONENT----------------------------------------
export default function LogosSection(): ReactElement {
  // Redux States
  const { isMovil } = useSelector((reducers: ReduxState) => reducers.appInfoReducer);
  return (
    <Row className={isMovil ? 'logos logos-ismovil' : 'logos'} justify="space-between">
      <Col className="FD" xs={24} sm={24} md={8} lg={8} xl={8}>
        <img src="/images/AcercaDe/FD.png" alt="" />
      </Col>
      <Col className="UBA" xs={24} sm={24} md={8} lg={8} xl={8}>
        <img src="/images/AcercaDe/UBA.png" alt="" />
      </Col>
      <Col className="EPJ" xs={24} sm={24} md={8} lg={8} xl={8}>
        <img src="/images/AcercaDe/EPJ.png" alt="" />
      </Col>
    </Row>
  );
}
