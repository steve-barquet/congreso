/* eslint-disable max-len */
// ---Dependencys
import { ReactElement } from 'react';
import { Row, Col } from 'antd';
// ---Components
import TwinButtons from 'Cont/CIDT/components/TwinButtons';

// ----------------------------------------COMPONENT----------------------------------------
export default function Content(): ReactElement {
  return (
    <Row className="text-cont">
      <Col span={24}>
        <h1>Acerca del congreso.</h1>
        <hr />
        <p>
          El curso está dirigido a abogados, contadores, administradores y
          economistas que estudian u operan desde diversos enfoques las
          problemáticas de la relación tributaria.
          <br />
          El objetivo principal es realizar un estado de la cuestión sobre las
          relaciones tributarias y su problemática actual, así como inducir la
          reflexión y crítica en los sistemas actuales para concluir con algunas
          propuestas en cada segmento.
          <br />
          El congreso es bimodal: presencial y vía zoom.
          <br />
          Presencial o Virtual se podrá acceder a todas las ponencias en vivo y se obtendrá una constancia expedida por las 3 Universidades convocantes.
          <br />
          - La modalidad presencial se realizará en el auditorio Ius Semper Loquitur en la Facultad de Derecho de la UNAM incluyendo un kit de participante.
          <br />
          - La modalidad virtual será mediante la plataforma Zoom y acceso a las conferencias grabadas hasta por 30 días.
        </p>
        <ul>
          <h1>CUOTA DE RECUPERACIÓN</h1>
          <li>a) Presencial (sujeto a disponibilidad hasta 100 plazas) $1,000.00 MX</li>
          <li>b) Modalidad virtual (Público en General) $800.00 MX</li>
          <li>c) Modalidad virtual (Comunidad UNAM) $600.00 MX</li>
          <p>
            <b>MEDIOS DE PAGO</b>
            <br />
            CUENTA: 00000037314
            <br />
            SUCURSAL:SUCURSAL REFORMA
            <br />
            CLABE: 113180000000373146
            <br />
            Paypal:
            {' '}
            <a
              href="https://www.paypal.com/paypalme/cidt22"
              target="_blank"
              rel="noreferrer"
            >
              paypal.me/cidt22
            </a>
          </p>
        </ul>
      </Col>
      <Col span={24}>
        <TwinButtons />
      </Col>
    </Row>
  );
}
