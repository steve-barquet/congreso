/* eslint-disable max-len */
// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';
// ---Components
import InputPre from 'Cont/Inscripcion/Components/Input-pre';
import Cuota from 'Cont/Programa/components/Cuota';

// ----------------------------------------COMPONENT----------------------------------------
export default function InscripcionCont(): ReactElement {
  return (
    <Row className="inscripciones-container" justify="center">
      <Col span={24}>
        <h1>PRE-REGISTRO</h1>
        <p>
          Al llenar este formulario le llegara un correo con la informacion y detalles del congreso,
          en caso de ser necesario estaremos en continuo contacto para ayudarle en su proceso de inscripcion.
          <br />
          Consulta nuestro aviso de privacidad (
          <a href="/files/Aviso_de_privacidad_Consultores_y _Asesores_CIDT.pdf" download>
            Aquí
          </a>
          )
        </p>
      </Col>
      <InputPre />
      <Cuota />
    </Row>
  );
}
