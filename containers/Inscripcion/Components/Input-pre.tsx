/* eslint-disable jsx-a11y/iframe-has-title */
// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';
// ---Redux
import { useSelector } from 'react-redux';
import { ReduxState } from '@Redux/globalReducers';

// ----------------------------------------COMPONENT----------------------------------------
export default function InputPre(): ReactElement {
  const { isMovil } = useSelector((reducers: ReduxState) => reducers.appInfoReducer);
  return (
    <Row className="input-section">
      <Col span={24}>
        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfxUQPoIfOERGuDL8H5oq7aEie0pgtgOfVZLAdYPguwdP5VZQ/viewform?embedded=true" width={isMovil ? 350 : 700} height={770} frameBorder={0} marginHeight={0} marginWidth={0}>Cargando…</iframe>
      </Col>
    </Row>
  );
}
