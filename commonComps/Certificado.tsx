// ---Dependencys
import { ReactElement } from 'react';
import { Col, Row } from 'antd';
// ---Components

// ----------------------------------------COMPONENT----------------------------------------
export default function Certificado(): ReactElement {
  return (
    <Row className="certi">
      <Col span={24}>
        <p>Certificado expedido por las 3 Universidades</p>
      </Col>
    </Row>
  );
}
