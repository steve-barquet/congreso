/* eslint-disable jsx-a11y/control-has-associated-label */
// ---Dependencys
import React, { ReactElement } from 'react';
// ----Interface
interface Props {
  callback: ()=>void;
  isVisible: boolean;
}
// ------------------------------------------ COMPONENT-----------------------------------------
const InvisibleButton = (props: Props): ReactElement => {
  const { callback, isVisible } = props;
  if (isVisible) {
    return (
      <button
        type="button"
        onClick={callback}
        tabIndex={0}
        className="invisible-button"
      />
    );
  }
  return (<></>);
};

export default InvisibleButton;
