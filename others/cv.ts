// ----------------------------------------- Presidencia -------------------------------------------
export const presidencia = [
  {
    nombre: 'Doctor Raúl Contreras Bustamante',
    curriculum: 'Es Licenciado en Derecho y Doctor por la Facultad de Derecho de la UNAM. Es Doctor en Derecho por la Universidad de Salamanca y posee además múltiples especializaciones y diplomados en Derecho, Ciencia Política y Administración Pública. Actualmente es Director de la Facultad de Derecho de la UNAM. Es Profesor Titular C de la Facultad de Derecho y merecedor del estímulo PRIDE Categoría C; el Sistema Nacional de Investigadores lo reconoce como Investigador Nacional Nivel I. Cuenta con una antigüedad académica de más 32 años, es coautor de 21 libros de estudios jurídicos diversos y autor de 4 obras.',
    img: '/images/CV/RAÚL_CONTRERAS_BUSTAMANTE.jpg'
  },
  {
    nombre: 'Doctora María Guadalupe Fernandez Ruiz',
    curriculum: 'Es licenciada, maestra y doctora en derecho por la UNAM. Es Profesora titular “C” en la Facultad de Derecho de la UNAM. Profesora por oposición de Derecho Administrativo. Ha fungido como directora del Seminario de Derecho Administrativo y como consejera técnica de la misma Facultad. Miembro del Claustro de Doctores de la Facultad de Derecho de la UNAM, de la Asociación Mexicana de Derecho Administrativo y de la Asociación Internacional de Derecho Administrativo. Ha publicado diversos artículos y libros especializados. Recipiendaria de la Cátedra extraordinaria Andrés Serra Rojas, así como de la Medalla Antonio Caso. Recibió el Premio Sor Juana Inés de la Cruz.',
    img: '/images/CV/MARÍA_GUADALUPE_FERNÁNDEZ RUIZ.jpg'
  }
];
// ----------------------------------------- Comite Organizador ------------------------------------
export const comitOrga = [
  {
    nombre: 'Maestra Gema Ayecac Jiménez',
    curriculum: 'Licenciada en Derecho por la UNAM. Cuenta con estudios de Maestría en Derecho en el ITAM y se encuentra doctorando en Mediación y Negociación en el Instituto de Mediación México. Es mediadora Privada con Fe Pública 520, Certificada por el Tribunal Superior de Justicia de la Ciudad de México y miembro del Colegio Nacional de Mediadores Certificados. Es docente en la División de Estudios de Posgrado de la Facultad de Derecho de la UNAM y en la FES Aragón.',
    img: '/images/CV/gemaAyecac.jpg'
  },
  {
    nombre: 'Maestro Ambrosio Michel Híguera',
    curriculum: 'Es Licenciado en Derecho por la Escuela Libre de Derecho y Maestro en Ciencias Penales por el INACIPE. Desempeñó diversos cargos en la Secretaría de Hacienda y Crédito Público, llegando a ocupar el cargo de Subprocurador Fiscal de la Federación. Es Docente, adjunto y titular, de diversas materias en diversas instituciones. Actualmente es catedrático y Coordinador del Diplomado de Delitos Fiscales en la Escuela Libre de Derecho. Autor de Diversas Obras especializadas en Derecho Penal Económico.',
    img: '/images/CV/ambrosioMichel.jpeg'
  },
  {
    nombre: 'Doctor Rogelio Zacarías Rodríguez Garduño',
    curriculum: 'Es Licenciado en Derecho por la UNAM. Cuenta con estudios de Maestría en Derecho Procesal Constitucional, Maestría en Ciencias Jurídicas por la Universidad Panamericana así como Doctorado en Derecho por la Universidad Panamericana. Realizó una Especialidad en Administración de Empresas de Servicio por el ITAM. Está Certificado por la International Air Transport Association en Ginebra, Suiza. Ha realizado diversos estudios de posgrado en Florida, USA, Montreal, Canadá, Buenos Aires, Argentina, entre otros. Es Catedrático en la Facultad de Derecho de la UNAM.',
    img: '/images/CV/rogelioZacaríasRodríguezGarduño.PNG'
  }
];
// ----------------------------------------- Comite Academico --------------------------------------
export const comitAcade = [
  {
    nombre: 'Doctora Rosa Carmen Rascón Gasca',
    curriculum: 'Doctora en Derecho. Profesora por oposición en la Facultad de Derecho de la Universidad Nacional Autónoma de México, con una trayectoria académica de 28 años. Ha desempeñado diversos cargos en la Administración Pública y cultiva como línea de investigación Derecho Administrativo. Actualmente funge como Coordinadora Académica de Maestría y Doctorado de la División de Estudios de Posgrado de la Facultad de Derecho de la UNAM.',
    img: '/images/CV/ROSA_CARMEN_RASCÓN_GASCA.jpg'
  },
  {
    nombre: ' Doctor Juan Manuel Álvarez Echagüe',
    curriculum: 'Abogado. Doctor en Derecho. Director del Observatorio de Derecho Penal Tributario de la Facultad de Derecho de la Universidad de Buenos Aires. Autor de tres libros sobre la materia Tributaria. Profesor de grado y Posgrado en Argentina y en el exterior.',
    img: '/images/CV/juanManuelÁlvarezEchagüe.jpg'
  },
  {
    nombre: 'Doctor José Manuel Almudí Cid',
    curriculum: 'Profesor Titular de Derecho Financiero y Tributario y Director de la Escuela de Práctica Jurídica de la Universidad Complutense de Madrid. Autor de cuatro monografías, coautor de dos manuales de la disciplina y autor de más de un centenar de artículos, capítulos de libros y comentarios jurisprudenciales publicados en revistas especializadas. Entre sus líneas prioritarias de investigación destacan la protección de los derechos y garantías de los contribuyentes, el Derecho tributario internacional y de la Unión Europea, las normas antiabuso y la imposición indirecta.',
    img: '/images/CV/almudiCid.png'
  }
];
// ----------------------------------------- Conferencistas ----------------------------------------
export const conferencistas = [
  {
    nombre: 'Maestra Lérida Alejandra Rodríguez Pastrana',
    curriculum: 'Es Licenciada en Derecho por la Universidad la Salle y Maestra en Derecho Fiscal en la Universidad Panamericana. Es Especialista en Justicia Administrativa por el Instituto del TFJFA. En la Administración Pública ha colaborado en la Administración General Jurídica del SAT, en la PRODECON como Delegada y Directora de Acuerdos Conclusivos y Gestión Institucional. Es docente en diversas Instituciones.',
    img: '/images/CV/léridaRodriguez.jpeg'
  },
  {
    nombre: 'Doctora Margarita Palomino Guerrero',
    curriculum: 'Es Licenciada en Derecho por la UNAM. Cuenta con estudios de maestría y doctorado por la UNAM. Directora del Seminario de Derecho Fiscal y Finanzas Públicas de la Facultad de Derecho UNAM y Consejera Universitaria. Miembro del Consejo Editorial de Instituto de la Judicatura Federal. Responsable del Área de Vinculación Interinstitucional del Instituto de Investigaciones Jurídicas de la UNAM. Miembro de la Comisión de acompañamiento a los trabajos de la UIF de la SHCP.',
    img: 'images/CV/margaritaPalomino.jpg'
  },
  {
    nombre: 'Doctor Juan Manuel Jiménez Illescas',
    curriculum: 'Abogado y doctor en Derecho por la Escuela Libre de Derecho. Magistrado de la Sala Superior del TFJA, del cual fue presidente. Se ha desempeñado como Administrador General de Recaudación y Subprocurador Fiscal Federal de Legislación y Consulta en la SHCP; Titular de la Unidad de Fiscalización y Cobranza del IMSS. Profesor titular de la cátedra de Derecho Fiscal en la Escuela Libre de Derecho. Autor de obras y diversos artículos en materia tributaria.',
    img: '/images/CV/juanManuelJiménelIllescas.jpg'
  },
  {
    nombre: 'Maestra Gema Ayecac Jiménez',
    curriculum: 'Licenciada en Derecho por la UNAM. Cuenta con estudios de Maestría en Derecho en el ITAM y se encuentra doctorando en Mediación y Negociación en el Instituto de Mediación México. Es mediadora Privada con Fe Pública 520, Certificada por el Tribunal Superior de Justicia de la Ciudad de México y miembro del Colegio Nacional de Mediadores Certificados. Es docente en la División de Estudios de Posgrado de la Facultad de Derecho de la UNAM y en la FES Aragón.',
    img: '/images/CV/gemaAyecac.jpg'
  },
  {
    nombre: 'Doctor Alejandro Zubimendi Cavia',
    curriculum: 'Licenciado en Derecho por la Universidad de Deusto y Máster en Derecho empresarial y en fiscalidad internacional. Es doctor en Derecho por la Universidad Complutense de Madrid. Actualmente es profesor asociado en el Instituto Superior de Derecho y Economía y en la Universidad Complutense de Madrid. Es secretario académico de la Escuela de Práctica Jurídica de la UCM. Es autor de múltiples artículos y capítulos de libros.',
    img: '/images/CV/alejandroZubimendiCavia.JPG'
  },
  {
    nombre: 'Doctora Marta Moreno Corte',
    curriculum: 'Docente e investigadora en la Universidad Complutense de Madrid. Doctora en Derecho con mención internacional y calificación sobresaliente cum laude. Máster en Estudios Avanzados de Derecho Financiero y Tributario por la Universidad Complutense de Madrid y la Universidad a distancia de Madrid. Ha realizado prácticas jurídicas en el Tribunal de Justicia de la Unión Europea. Ha realizado estancias de investigación en el Tribunal Europeo de Derechos Humanos y en la Universidad de Luxemburgo.',
    img: '/images/CV/martaMoreno.png'
  },
  {
    nombre: 'Doctora Sonia Venegas Álvarez',
    curriculum: 'Licenciada en Derecho y Doctora en Derecho fiscal, por la UNAM. Profesora titular en la Facultad de Derecho de la UNAM. Fue Directora del Seminario de Derecho Administrativo en la Facultad de Derecho de la UNAM y actualmente desempeña el cargo de Secretaria Académica en la misma Facultad. Miembro del Sistema Nacional de Investigadores CONACYT. Recipiendaria de las medallas: Ignacio L. Vallarta, al mérito Universitario, y Sor Juana Inés de la Cruz; de la cátedra extraordinaria "Félix Pichardo Estrada"; miembro del Consejo Académico del Instituto de Especialización en Justicia Administrativa y Fiscal, del Tribunal de lo Contencioso Administrativo hoy Tribunal de Justicia Administrativa de la Ciudad de México; autora de libros y artículos jurídicos.',
    img: '/images/CV/soniaVenegas.jpg'
  },
  {
    nombre: ' Doctor Manuel Luciano Hallivis Pelayo',
    curriculum: 'Doctor en Derecho por la UNAM, Maestro en Derecho por la Tulane University y la University of Michigan. Magistrado de la Sala Superior del TFJA. Ha ocupado diversos cargos en la Administración Pública Federal en la SHCP, el IMSS, Fertimex y la Contaduría Mayor de Hacienda. Es recipiendario de diversos galardones nacionales e internacionales y autor de diversos libros y artículos. Es catedrático en la UNAM, la Universidad de Salamanca y la Universidad Panamericana, entre otras.',
    img: '/images/CV/Manuel_Luciano_Hallivis_Pelayo.jpg'
  },
  {
    nombre: 'Maestro Gabriel Baltazar Pedraza',
    curriculum: 'Abogado por el ITAM, Maestro en Derecho por la Universidad de California, Berkeley, Litigante especializado en materia fiscal y constitucional, Profesor de Amparo y Recursos Administrativos en UMich.',
    img: '/images/CV/gabrielBaltazar.jpeg'
  },
  {
    nombre: 'Doctor Cristian José Billardi',
    curriculum: 'Es Doctor en Derecho por la UNIPA- Palermo Italia. Docente de derecho Tributario y Derecho Deportivo así como Profesor adjunto de Derecho Tributario U.B.A. (Argentina) y de Posgrado en diversos Países de América Latina e Italia. Desempeña como Secretario de la Associazione Italiana per il Diritto Tributario Latinoamericano. Miembro activo de la Asociación Argentina de Estudios Fiscales. Es responsable de la redacción de la Revista Diritto e Pratica Tributaria Internazionale.',
    img: '/images/CV/Cristian_Jose.jpg'
  },
  {
    nombre: 'Juez Pablo Garbarino',
    curriculum: 'Es abogado por la Universidad de Buenos Aires. Actualmente desempeña cargo como Juez del Tribunal Fiscal de la Nación en la Vocalía de la 16ª y anteriormente como Secretario Letrado de la Corte Suprema de Justicia de la Nación. Es Director del Posgrado de Derecho Aduanero, Cambiario y de Comercio Exterior en la Universidad de Buenos Aires. Es Profesor Titular en diversas universidades como la Universidad Nacional de Rosario, Universidad Católica de Argentina, Universidad Nacional de Comahue. Es Coordinador de la Comisión de Tribunales Fiscales de la Asociación Argentina de Estudios Fiscales.',
    img: '/images/CV/pabloGarbarino.jpeg'
  },
  {
    nombre: 'Senadora Minerva Hernández Ramos',
    curriculum: 'Senadora de la República. Ha forjado su trayectoria dentro de las finanzas públicas al integrar las comisiones de Hacienda y Crédito Público, Presupuesto y Cuenta Pública, Economía, Seguridad Social y Disciplina Financiera, en ambas Cámaras del Congreso de la Unión en diversas legislaturas. Fue Secretaria de Finanzas de Tlaxcala y Subprocuradora en PRODECON. Contadora y Administradora; con estudios de maestría y doctorado en Administración Pública Estatal y Municipal, Prevención y Combate a la Corrupción, Estrategias Anticorrupción y Estado de Derecho y Ciencias de lo Fiscal.',
    img: '/images/CV/minervaHernándezRamos.jpg'
  },
  {
    nombre: 'Doctor Carlos Javier Verduzco Reina',
    curriculum: 'Es Licenciado en Derecho por la UNAM, Maestro en Derecho Fiscal y Administrativo y Doctor en Derecho por el Centro Universitario de Estudios Jurídicos. Profesor por oposición a la cátedra de Derecho Fiscal en la Facultad de Derecho de la UNAM. Miembro Numerario de la Academia Mexicana de Derecho Fiscal. Miembro de la Asociación Nacional de Doctores en Derecho y autor de diversos libros y artículos.',
    img: '/images/CV/carlosVerduzcoReina.png'
  },
  {
    nombre: 'Abogado Manuel de Allende',
    curriculum: 'Es Abogado Especialista en Derecho Penal Económico, Especialista en Derecho Tributario.Profesor de Derecho Penal Económico en la Maestría en Derecho Penal Tributario de la Universidad de Buenos Aires. Es Director de la sala fiscal del Colegio de Abogados de Córdoba, Argentina. Miembro de la Fundación de Estudios e Investigaciones tributarias. Es Profesor en la Especialización en Derecho Penal Tributario y Económico, y en el Programa en Compliance y Prevención de la Corrupción Corporativa, Universidad Siglo 21, Argentina. Autor de diversas publicaciones.',
    img: '/images/CV/manuelDeAllende.jpg'
  },
  {
    nombre: 'Abogado Fabián Ávila Torres',
    curriculum: 'Es Licenciado en Derecho por la Escuela Libre de Derecho. Fue Subdirector de Área de la Dirección General Jurídica en la PGR y ocupó diversos cargos en la Procuraduría Fiscal de la Federación, siendo el último el de Director General de Delitos Financieros. Coautor de las obras “Ley Antilavado. Ley Federal para la Prevención e Identificación de Operaciones con Recursos de Procedencia Ilícita y su Reglamento Comentados” y “Lavado de Dinero”. Profesor invitado en la Escuela Libre de Derecho y en la Universidad Panamericana Campus Guadalajara.',
    img: '/images/CV/fabiánÁvilaTorres.png'
  },
  {
    nombre: 'Doctor Sebastián P. Espeche',
    curriculum: 'Abogado y Especialista en Derecho Tributario por la Universidad de Buenos Aires.  Es Doctor en Ciencias Jurídicas por la Universidad del Salvador. Es profesor titular en la Universidad Católica de Salta y profesor de posgrado en la Maestría de Derecho Tributario de la Universidad de Buenos Aires. Entre sus numerosas publicaciones se destaca el libro “Curso de Derecho Financiero”.',
    img: '/images/CV/sebastianEspeche.png'
  },
  {
    nombre: 'Doctor Rogelio Zacarías Rodríguez Garduño',
    curriculum: 'Es Licenciado en Derecho por la UNAM. Cuenta con estudios de Maestría en Derecho Procesal Constitucional, Maestría en Ciencias Jurídicas por la Universidad Panamericana así como Doctorado en Derecho por la Universidad Panamericana. Realizó una Especialidad en Administración de Empresas de Servicio por el ITAM. Está Certificado por la International Air Transport Association en Ginebra, Suiza. Ha realizado diversos estudios de posgrado en Florida, USA, Montreal, Canadá, Buenos Aires, Argentina, entre otros. Es Catedrático en la Facultad de Derecho de la UNAM.',
    img: '/images/CV/rogelioZacaríasRodríguezGarduño.PNG'
  },
  {
    nombre: 'Maestro  Jaime Armando García  Arredondo',
    curriculum: 'Licenciado en Derecho por la Facultad de Derecho de la UNAM con mención honorifica, con estudios de posgrado en materia fiscal con mención honorifica en la misma universidad. Es catedrático en la Universidad Nacional Autónoma de México y diversas universidades a nivel licenciatura y posgrado. Es autor de libros especializados en materia fiscal y aduanera, así como artículos en diversos temas de sus áreas de especialización.',
    img: '/images/CV/jaimeArredondo.jpg'
  },
  {
    nombre: 'Maestro Rogelio D. Ibarra',
    curriculum: 'Se ha desempeñado como asesor parlamentario en las Comisiones de Hacienda y Crédito Público, de Presupuesto y Cuenta Pública, de Disciplina Financiera y de Economía, en temas como finanzas públicas, política fiscal, regulación financiera y derecho penal tributario; así como en mecanismos de transparencia y rendición de cuentas y control de la corrupción. Es licenciado en derecho con estudios de posgrado en finanzas públicas, tax compliance, y derecho penal; ha sido expositor en diversos foros nacionales e internacionales.',
    img: '/images/CV/rogelioDIbarra.jpg'
  },
  {
    nombre: 'Maestro Ambrosio Michel Híguera',
    curriculum: 'Profesor Titular de Derecho Financiero y Tributario y Director de la Escuela de Práctica Jurídica de la Universidad Complutense de Madrid. Autor de cuatro monografías, coautor de dos manuales de la disciplina y autor de más de un centenar de artículos, capítulos de libros y comentarios jurisprudenciales publicados en revistas especializadas. Entre sus líneas prioritarias de investigación destacan la protección de los derechos y garantías de los contribuyentes, el Derecho tributario internacional y de la Unión Europea, las normas antiabuso y la imposición indirecta.',
    img: '/images/CV/ambrosioMichel.jpeg'
  },
  {
    nombre: 'Doctor José Manuel Almudí Cid',
    curriculum: 'Profesor Titular de Derecho Financiero y Tributario y Director de la Escuela de Práctica Jurídica de la Universidad Complutense de Madrid. Autor de cuatro monografías, coautor de dos manuales de la disciplina y autor de más de un centenar de artículos, capítulos de libros y comentarios jurisprudenciales publicados en revistas especializadas. Entre sus líneas prioritarias de investigación destacan la protección de los derechos y garantías de los contribuyentes, el Derecho tributario internacional y de la Unión Europea, las normas antiabuso y la imposición indirecta.',
    img: '/images/CV/almudiCid.png'
  },
  {
    nombre: 'Doctor Luis Manuel Alonso González',
    curriculum: 'Es Catedrático de Derecho Financiero y Tributario de la Universidad de Barcelona, donde dirige el Departamento de Derecho Financiero y Tributario. En el mundo universitario ha desempeñado otros cargos como Decano de la Facultad de Derecho de la Universidad Internacional de Cataluña. También ha sido Presidente de la Ponencia Jurídica de Fomento del Trabajo Nacional. Ha desarrollado una amplísima actividad científica. Ha publicado varias decenas de libros y más de un centenar de artículos, siendo uno de sus temas predilectos el fraude y el delito fiscal.',
    img: '/images/CV/luisAlonso.jpg'
  },
  {
    nombre: ' Doctor Juan Manuel Álvarez Echagüe',
    curriculum: 'Abogado. Doctor en Derecho. Director del Observatorio de Derecho Penal Tributario de la Facultad de Derecho de la Universidad de Buenos Aires. Autor de tres libros sobre la materia Tributaria. Profesor de grado y Posgrado en Argentina y en el exterior.',
    img: '/images/CV/juanManuelÁlvarezEchagüe.jpg'
  }
];
