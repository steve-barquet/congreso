export const dataDay19 = [
  {
    initHrs: '10:00 a 10:30',
    evento: 'Bienvenida y apertura del Congreso',
    participante: 'Doctora María Guadalupe Fernández Ruiz / Maestro Ambrosio Michel Higuera  Doctora Rosa Carmen Rascón Gasca / Maestra Gema Ayecac Jiménez'
  },
  {
    tituloBold: 'Panel UNO. ADMINISTRACION TRIBUTARIA EN MÉXICO',
    modera: 'Modera Doctor Rogelio Rodríguez Garduño',
    initHrs: '10:30 a 10:50',
    evento: 'Razón de Negocios y Materialidad en México',
    participante: 'Maestra Lérida Alejandra Rodríguez Pastrana',
    puesto: 'Universidad Panamericana'
  },
  {
    initHrs: '10:50 a 11:10',
    evento: 'Inteligencia Artificial y Administración Tributaria',
    participante: 'Doctora Margarita Palomino Guerrero',
    puesto: 'UNAM'
  },
  {
    initHrs: '11:10 a 11:30',
    evento: 'El Juicio de Lesividad en México',
    participante: 'Doctor Juan Jiménez Illescas',
    puesto: 'Tribunal Federal de Justicia Administrativa'
  },
  {
    initHrs: '11:30 a 11:50',
    evento: 'Preguntas y Debate',
    participante: ''
  },
  {
    initHrs: '11:50 a 12:10',
    evento: 'Coffee Break',
    participante: ''
  },
  {
    tituloBold: 'Panel DOS.  RELACIÓN COOPERATIVA Y TAX COMPLIANCE',
    modera: 'Modera Maestro Joel Mora Vázquez',
    initHrs: '12:30 a 12:50',
    evento: 'Relación Cooperativa y MASC en Materia Tributaria',
    participante: 'Maestra Gema Ayecac Jiménez',
    puesto: 'UNAM'
  },
  {
    initHrs: '12:50 a 13:10',
    evento: 'Gobierno Corporativo y Tax Compliance',
    participante: 'Doctor Alejandro Zubimendi Cavia',
    puesto: 'Universidad Complutense de Madrid'
  },
  {
    initHrs: '13:10 a 13:30',
    evento: 'Tax compliance y su funcion como atenuante o excluyente de pena',
    participante: 'Doctora Marta Moreno Corte',
    puesto: 'Universidad Complutense de Madrid'

  },
  {
    initHrs: '13:30 a 13:50',
    evento: 'PRODECON y su Función como Ombudsperson en México',
    participante: 'Doctora Sonia Venegas Álvarez',
    puesto: 'UNAM'
  },
  {
    initHrs: '13:50 a 14:10',
    evento: 'Preguntas y Debate',
    participante: ''
  },
  {
    breakTime: 'RECESO',
    tituloBold: 'Panel TRES. DERECHOS Y RELACION TRIBUTARIA',
    modera: 'Modera Maestro Rodrigo A.  Mejía Ornelas',
    initHrs: '16:00 a 16:20',
    evento: 'Tributación en la Economía Global: BEPS e Impuesto Global a las Multinacionales',
    participante: ' Doctor Manuel Luciano Hallivis Pelayo',
    puesto: 'Tribunal Federal de Justicia Administrativa'
  },
  {
    initHrs: '16:20 a 16:50',
    evento: 'Derecho a la Privacidad ante el Uso de Tecnologías de la Información en Materia Tributaria',
    participante: 'Maestro Gabriel Baltazar Pedraza',
    puesto: 'ITAM, Berkeley'
  },
  {
    initHrs: '16:50 a 17:10',
    evento: 'Principio de Reserva de Ley: entre Algoritmos y Soft-law',
    participante: 'Doctor Cristian José Billardi',
    puesto: 'Universidad de Buenos Aires'
  },
  {
    initHrs: '17:10 a 17:30',
    evento: 'Control de Convencionalidad en Materia Tributaria',
    participante: 'Doctor Pablo Garbarino',
    puesto: 'Universidad de Buenos Aires'
  },
  {
    initHrs: '17:30 a 18:00',
    evento: 'Preguntas y Debate',
    participante: ''
  }
];
export const dataDay20 = [
  {
    tituloBold: 'Panel CUATRO. ECONOMÍA POLITICA Y RELACIÓN TRIBUTARIA',
    modera: 'Modera  Maestra Marisol Velazquez Copado',
    initHrs: '10:00 a 10:20',
    evento: 'Política Económica Tributaria en el Entorno Post Pandemia',
    participante: 'Senadora Minerva Hernández Ramos',
    puesto: 'Senado de la República'
  },
  {
    initHrs: '10:20 a 10:40',
    evento: 'Régimen Financiero y Eficacia Recaudatoria',
    participante: 'Doctor Carlos Javier Verduzco Reina',
    puesto: 'UNAM'
  },
  {
    initHrs: '10:40 a 11:00',
    evento: 'Tendencias  en Política Penal Tributaria. (DO y Prisión Preventiva)',
    participante: 'Maestro Manuel de Allende',
    puesto: 'Universidad de Buenos Aires'
  },
  {
    initHrs: '11:00 a 11:20',
    evento: 'La Prisión Preventiva en los Delitos Fiscales como Política Criminal en México',
    participante: ' Abogado Fabián Ávila Torres',
    puesto: 'Escuela Libre de Derecho'
  },
  {
    initHrs: '11:20 a 11:30',
    evento: 'Preguntas y Debate',
    participante: ''
  },
  {
    initHrs: '11:30 A 11:50',
    evento: 'Coffee Break',
    participante: ''
  },
  {
    tituloBold: 'Panel CINCO.   POLITICA TRIBUTARIA',
    modera: 'Modera Maestro Antemio Carrillo Sasso',
    initHrs: '11:50 a 12:10',
    evento: 'Derechos Humanos de los Contribuyentes y la Utilidad Pública de la Recaudación',
    participante: 'Doctor Sebastián Espeche',
    puesto: 'Universidad de Buenos Aires'
  },
  {
    initHrs: '12:10 a 12:30',
    evento: 'Parámetro de Regularidad en Materia Tributaria',
    participante: 'Doctor Rogelio Z. Rodríguez Garduño',
    puesto: 'UNAM'
  },
  {
    initHrs: '12:30 a 12:50',
    evento: 'Doble Tributación y Evasión Fiscal',
    participante: 'Maestro  Jaime Armando García  Arredondo',
    puesto: 'UNAM'
  },
  {
    initHrs: '12:50 a 13:10',
    evento: 'Análisis de las Estrategias de Recaudación: Discurso Oficial y Política Pública',
    participante: 'Maestro Rogelio Ibarra',
    puesto: 'Senado de la República'
  },
  {
    initHrs: '13:10 a 13:30',
    evento: 'Preguntas y Debate',
    participante: ''
  },
  {
    breakTime: 'RECESO',
    tituloBold: 'Panel SEIS. DERECHO PENAL TRIBUTARIO',
    modera: 'Modera Maestra Gema Ayecac ',
    initHrs: '15:30 a 15:50',
    evento: 'Politica Penal Tributaria en México',
    participante: 'Maestro Ambrosio Michel',
    puesto: 'Senado de la República'
  },
  {
    initHrs: '15:50 a 16:10',
    evento: 'Política Penal Tributaria Experiencia Comparada en España',
    participante: ' Doctor José Manuel Almudí Cid',
    puesto: 'Universidad Complutense de Madrid'
  },
  {
    initHrs: '16:10 a 16:30',
    evento: 'Desafíos del  Derecho Penal Tributario ante el Fraude Fiscal',
    participante: 'Doctor Luis Manuel Alonso González',
    puesto: 'Universidad de Barcelona'
  },
  {
    initHrs: '16:30 a 17:00',
    evento: 'Conferencia de Cierre Reflexiones finales',
    participante: 'Doctor Juan Manuel Álvarez Echagüe',
    puesto: 'Universidad de Buenos Aires'
  },
  {
    initHrs: '17:00 A 17:20',
    evento: 'Preguntas y Debate',
    participante: ''
  },
  {
    initHrs: '17:20',
    evento: 'Clausura del Congreso',
    participante: 'Doctor Rogelio Rodriguez'
  }
];
// ---ñ
